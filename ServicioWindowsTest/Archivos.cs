﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ServicioWindowsTest
{
    partial class Archivos : ServiceBase
    {

        bool estado = false;
        public Archivos()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: agregar código aquí para iniciar el servicio.
            Lapso.Start();
        }

        protected override void OnStop()
        {
            // TODO: agregar código aquí para realizar cualquier anulación necesaria para detener el servicio.
            Lapso.Stop();
        }

        private void Lapso_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            if (estado) return;

            EventLog.WriteEntry("Proceso de inicio de copiado", EventLogEntryType.Information);

            var rutaOrigen = ConfigurationSettings.AppSettings["RutaOrigen"].ToString();
            var rutaDestino = ConfigurationSettings.AppSettings["RutaDestino"].ToString();

            try
            {
                estado = true;              
                

                DirectoryInfo di = new DirectoryInfo(rutaOrigen);

                foreach (var archivo in di.GetFiles("*",SearchOption.AllDirectories))
                {

                    if (File.Exists($"{ rutaDestino }{ archivo.Name }"))
                    {
                        File.SetAttributes($"{ rutaDestino }{ archivo.Name }", FileAttributes.Normal);
                        File.Delete($"{ rutaDestino }{ archivo.Name }");
                    }

                    File.Copy($"{ rutaOrigen }{ archivo.Name }", $"{ rutaDestino }{ archivo.Name }");
                    File.SetAttributes($"{ rutaDestino }{ archivo.Name }", FileAttributes.Normal);

                    if (File.Exists($"{ rutaDestino }{ archivo.Name }"))
                    {
                        EventLog.WriteEntry("Archivo copiado correctamente", EventLogEntryType.Information);

                    }
                    else
                    {
                        EventLog.WriteEntry("El archivo no ha sido copiado", EventLogEntryType.Information);

                    }

                }

                EventLog.WriteEntry("Se finalizó proceso de copiado", EventLogEntryType.Information);


            }
            catch (Exception ex)
            {

                EventLog.WriteEntry(ex.Message, EventLogEntryType.Error);

            }

            estado = false;


        }
    }
}
